﻿using System;
using System.Linq;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using uMod.Libraries;
using uMod.Plugins;

namespace uMod.Rust.Libraries
{
    /// <summary>
    /// A library containing functions for adding console and chat commands
    /// </summary>
    [Obsolete]
    public class Command : Library
    {
        public Command(IApplication application) : base(application)
        {
        }

        /// <summary>
        /// Adds a chat command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        [LibraryFunction("AddChatCommand")]
        public void AddChatCommand(string command, Plugin plugin, string callback)
        {
            CommandDefinition commandDefinition = new CommandDefinition(command);
            plugin.Commands.Add(new CommandInfo(commandDefinition, plugin.Commands.CreateCallback(commandDefinition, callback)));
        }

        /// <summary>
        /// Adds a chat command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void AddChatCommand(string command, Plugin plugin, Action<BasePlayer, string, string[]> callback)
        {
            CommandDefinition commandDefinition = new CommandDefinition(command);
            plugin.Commands.Add(new CommandInfo(commandDefinition, plugin.Commands.CreateCallback(commandDefinition, delegate (IPlayer player, IArgs args)
            {
                callback.Invoke(player.Object as BasePlayer, args.Command, args.Values.Cast<string>().ToArray());
                return CommandState.Completed;
            })));
        }

        /// <summary>
        /// Adds a console command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        [LibraryFunction("AddConsoleCommand")]
        public void AddConsoleCommand(string command, Plugin plugin, string callback)
        {
            AddConsoleCommand(command, plugin, arg => plugin.CallHook(callback, arg) != null);
        }

        /// <summary>
        /// Adds a console command with a delegate callback
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void AddConsoleCommand(string command, Plugin plugin, Func<ConsoleSystem.Arg, bool> callback)
        {
            CommandDefinition commandDefinition = new CommandDefinition(command);
            plugin.Commands.Add(new CommandInfo(commandDefinition, plugin.Commands.CreateCallback(commandDefinition, delegate (IPlayer player, IArgs args)
            {
                if (args.TryGetContext(typeof(ConsoleSystem.Arg), out ConsoleSystem.Arg consoleArg))
                {
                    return callback.Invoke(consoleArg) ? CommandState.Completed : CommandState.Canceled;
                }

                return CommandState.Canceled;
            })));
        }

        /// <summary>
        /// Removes a previously registered chat command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        [LibraryFunction("RemoveChatCommand")]
        public void RemoveChatCommand(string command, Plugin plugin)
        {
            plugin.Commands.Remove(command);
        }

        /// <summary>
        /// Removes a previously registered console command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        [LibraryFunction("RemoveConsoleCommand")]
        public void RemoveConsoleCommand(string command, Plugin plugin)
        {
            plugin.Commands.Remove(command);
        }
    }
}

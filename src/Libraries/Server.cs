﻿using System;
using uMod.Common;
using uMod.Libraries;
using uMod.Text;

namespace uMod.Rust.Libraries
{
    [Obsolete]
    public class Server : Library
    {
        public Server(IApplication application) : base(application)
        {
        }

        #region Chat and Commands

        /// <summary>
        /// Broadcasts the specified chat message and prefix to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="avatarId"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Broadcast(string message, string prefix, ulong avatarId = 0, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                ConsoleNetwork.BroadcastToAllClients("chat.add", 2, avatarId, prefix != null ? $"{prefix}: {message}" : message);
            }
        }

        /// <summary>
        /// Broadcasts the specified chat message to all players
        /// </summary>
        /// <param name="message"></param>
        /// <param name="avatarId"></param>
        public void Broadcast(string message, ulong avatarId = 0) => Broadcast(message, null, avatarId);

        /// <summary>
        /// Runs the specified server command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            ConsoleSystem.Run(ConsoleSystem.Option.Server, command, args);
        }

        #endregion Chat and Commands
    }
}

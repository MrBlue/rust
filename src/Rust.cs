using ConVar;
using Facepunch;
using Network;
using Rust.Ai;
using Rust.Ai.HTN;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using UnityEngine;

namespace uMod.Rust
{
    /// <summary>
    /// The core Rust plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Rust : Plugin
    {
        #region Initialization

        internal static ulong chatAvatar = 76561198127163614; // TODO: Implement setting for avatar

        private static readonly RustProvider Universal = RustProvider.Instance;
        private static readonly string ipPattern = @":{1}[0-9]{1}\d*";

        private bool isPlayerTakingDamage;
        private bool serverInitialized;

        /// <summary>
        /// Initializes a new instance of the Rust class
        /// </summary>
        public Rust()
        {
            // Set plugin info attributes
            Title = "Rust";
            Author = RustExtension.AssemblyAuthors;
            Version = RustExtension.AssemblyVersion;
        }

        #endregion Initialization

        #region Modifications

        // Set default values for command-line arguments and hide output
        [Hook("IOnRunCommandLine")]
        private object IOnRunCommandLine()
        {
            foreach (KeyValuePair<string, string> pair in Facepunch.CommandLine.GetSwitches())
            {
                string value = pair.Value;
                if (value == "")
                {
                    value = "1";
                }

                string str = pair.Key.Substring(1);
                ConsoleSystem.Option options = ConsoleSystem.Option.Unrestricted;
                options.PrintOutput = false;
                ConsoleSystem.Run(options, str, value);
            }
            return true;
        }

        #endregion Modifications

        #region Entity Hooks

        /// <summary>
        /// Called when a BaseCombatEntity takes damage
        /// This is used to call OnEntityTakeDamage for anything other than a BasePlayer
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="hitInfo"></param>
        /// <returns></returns>
        [Hook("IOnBaseCombatEntityHurt")]
        private object IOnBaseCombatEntityHurt(BaseCombatEntity entity, HitInfo hitInfo)
        {
            return entity is BasePlayer ? null : Interface.CallHook("OnEntityTakeDamage", entity, hitInfo);
        }

        private int GetPlayersSensed(NPCPlayerApex npc, Vector3 position, float distance, BaseEntity[] targetList)
        {
            return BaseEntity.Query.Server.GetInSphere(position, distance, targetList,
                entity =>
                {
                    BasePlayer target = entity as BasePlayer;
                    object callHook = target != null && npc != null && target != npc ? Interface.CallHook("OnNpcTarget", npc, target) : null;
                    if (callHook != null)
                    {
                        foreach (Memory.SeenInfo seenInfo in npc.AiContext.Memory.All)
                        {
                            if (seenInfo.Entity == target)
                            {
                                npc.AiContext.Memory.All.Remove(seenInfo);
                                break;
                            }
                        }

                        foreach (Memory.ExtendedInfo extendedInfo in npc.AiContext.Memory.AllExtended)
                        {
                            if (extendedInfo.Entity == target)
                            {
                                npc.AiContext.Memory.AllExtended.Remove(extendedInfo);
                                break;
                            }
                        }
                    }

                    return callHook == null && target.isServer && !target.IsSleeping() && !target.IsDead() && target.Family != npc.Family;
                });
        }

        /// <summary>
        /// Called when an Apex NPC player tries to target an entity based on closeness
        /// </summary>
        /// <param name="npc"></param>
        /// <returns></returns>
        [Hook("IOnNpcSenseClose")]
        private object IOnNpcSenseClose(NPCPlayerApex npc)
        {
            NPCPlayerApex.EntityQueryResultCount = GetPlayersSensed(npc, npc.ServerPosition, npc.Stats.CloseRange, NPCPlayerApex.EntityQueryResults);
            return true;
        }

        /// <summary>
        /// Called when an Apex NPC player tries to target an entity based on vision
        /// </summary>
        /// <param name="npc"></param>
        /// <returns></returns>
        [Hook("IOnNpcSenseVision")]
        private object IOnNpcSenseVision(NPCPlayerApex npc)
        {
            NPCPlayerApex.PlayerQueryResultCount = GetPlayersSensed(npc, npc.ServerPosition, npc.Stats.VisionRange, NPCPlayerApex.PlayerQueryResults);
            return true;
        }

        /// <summary>
        /// Called when an Apex NPC player (i.e. murderer) tries to target an entity
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        [Hook("IOnNpcTarget")]
        private object IOnNpcTarget(NPCPlayerApex npc, BaseEntity target)
        {
            if (Interface.CallHook("OnNpcTarget", npc, target) != null)
            {
                return 0f;
            }

            return null;
        }

        /// <summary>
        /// Called when an HTN NPC player (old scientist) tries to target an entity
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="target"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        [Hook("IOnNpcTarget")]
        private object IOnNpcTarget(IHTNAgent npc, BasePlayer target, int index)
        {
            if (npc != null && Interface.CallHook("OnNpcTarget", npc.Body, target) != null)
            {
                npc.AiDomain.NpcContext.PlayersInRange.RemoveAt(index);
                npc.AiDomain.NpcContext.BaseMemory.Forget(0f); // Unsure if still needed
                npc.AiDomain.NpcContext.BaseMemory.PrimaryKnownEnemyPlayer.PlayerInfo.Player = null; // Unsure if still needed
                return true;
            }

            return null;
        }

        /// <summary>
        /// Called when an NPC animal tries to target an entity
        /// </summary>
        /// <param name="npc"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        [Hook("IOnNpcTarget")]
        private object IOnNpcTarget(BaseNpc npc, BaseEntity target)
        {
            if (Interface.CallHook("OnNpcTarget", npc, target) != null)
            {
                npc.SetFact(BaseNpc.Facts.HasEnemy, 0);
                npc.SetFact(BaseNpc.Facts.EnemyRange, 3);
                npc.SetFact(BaseNpc.Facts.AfraidRange, 1);
                npc.AiContext.EnemyPlayer = null;
                npc.AiContext.LastEnemyPlayerScore = 0f;
                npc.playerTargetDecisionStartTime = 0f;
                return 0f;
            }

            return null;
        }

        #endregion Entity Hooks

        #region Item Hooks

        /// <summary>
        /// Called when an item loses durability
        /// </summary>
        /// <param name="item"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [Hook("IOnLoseCondition")]
        private object IOnLoseCondition(Item item, float amount)
        {
            // Call hook for plugins
            Interface.CallHook("OnLoseCondition", item, amount);

            float condition = item.condition;
            item.condition -= amount;
            if (item.condition <= 0f && item.condition < condition)
            {
                item.OnBroken();
            }

            return true;
        }

        #endregion Item Hooks

        #region Player Hooks

        /// <summary>
        /// Called when a player attempts to pickup a DoorCloser entity
        /// </summary>
        /// <param name="player"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [Hook("ICanPickupEntity")]
        private object ICanPickupEntity(BasePlayer player, DoorCloser entity)
        {
            return Interface.CallHook("CanPickupEntity", player, entity) is bool result && !result ? (object)true : null;
        }

        /// <summary>
        /// Called when a BasePlayer is attacked
        /// This is used to call OnEntityTakeDamage for a BasePlayer when attacked
        /// </summary>
        /// <param name="player"></param>
        /// <param name="hitInfo"></param>
        [Hook("IOnBasePlayerAttacked")]
        private object IOnBasePlayerAttacked(BasePlayer player, HitInfo hitInfo)
        {
            if (!serverInitialized || player == null || hitInfo == null || player.IsDead() || isPlayerTakingDamage || player is NPCPlayer)
            {
                return null;
            }

            if (Interface.CallHook("OnEntityTakeDamage", player, hitInfo) != null)
            {
                return true;
            }

            isPlayerTakingDamage = true;
            try
            {
                player.OnAttacked(hitInfo);
            }
            finally
            {
                isPlayerTakingDamage = false;
            }
            return true;
        }

        /// <summary>
        /// Called when a BasePlayer is hurt
        /// This is used to call OnEntityTakeDamage when the player was hurt without being attacked
        /// </summary>
        /// <param name="player"></param>
        /// <param name="hitInfo"></param>
        /// <returns></returns>
        [Hook("IOnBasePlayerHurt")]
        private object IOnBasePlayerHurt(BasePlayer player, HitInfo hitInfo)
        {
            return isPlayerTakingDamage ? null : Interface.CallHook("OnEntityTakeDamage", player, hitInfo);
        }

        /// <summary>
        /// Called when a server group is set for an ID (i.e. banned)
        /// </summary>
        /// <param name="steamId"></param>
        /// <param name="group"></param>
        /// <param name="name"></param>
        /// <param name="reason"></param>
        [Hook("IOnServerUsersSet")]
        private void IOnServerUsersSet(ulong steamId, ServerUsers.UserGroup group, string name, string reason)
        {
            if (serverInitialized)
            {
                string playerId = steamId.ToString();
                IPlayer player = Players.FindPlayerById(playerId);

                if (group == ServerUsers.UserGroup.Banned)
                {
                    if (player != null)
                    {
                        // Call hook for plugins
                        Interface.CallHook("OnPlayerBanned", player, reason);
                    }
                    Interface.CallHook("OnPlayerBanned", name, playerId, player?.Address ?? "0", reason);
                    Interface.CallDeprecatedHook("OnUserBanned", "OnPlayerBanned", new DateTime(2020, 6, 1), name, playerId, player?.Address ?? "0", reason);
                }
            }
        }

        /// <summary>
        /// Called when a server group is removed for an ID (i.e. unbanned)
        /// </summary>
        /// <param name="steamId"></param>
        [Hook("IOnServerUsersRemove")]
        private void IOnServerUsersRemove(ulong steamId)
        {
            if (serverInitialized)
            {
                string playerId = steamId.ToString();
                IPlayer player = Players.FindPlayerById(playerId);

                if (ServerUsers.Is(steamId, ServerUsers.UserGroup.Banned))
                {
                    if (player != null)
                    {
                        // Call hook for plugins
                        Interface.CallHook("OnPlayerUnbanned", player);
                    }
                    Interface.CallHook("OnPlayerUnbanned", player?.Name ?? "Unnamed", playerId, player?.Address ?? "0"); // TODO: Localization
                    Interface.CallDeprecatedHook("OnUserUnbanned", "OnPlayerUnbanned", new DateTime(2020, 6, 1), player?.Name ?? "Unnamed", playerId, player?.Address ?? "0"); // TODO: Localization
                }
            }
        }

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        [Hook("IOnUserApprove")]
        private object IOnUserApprove(Connection connection)
        {
            string playerName = connection.username;
            string playerId = connection.userid.ToString();
            string ipAddress = Regex.Replace(connection.ipaddress, ipPattern, string.Empty);

            Players.PlayerJoin(playerId, playerName);

            if (Interface.uMod.Auth.Configuration.Groups.AutomaticGrouping)
            {
                // Add player to default groups
                GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
                if (!permission.UserHasGroup(playerId, defaultGroups.Players))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Players);
                }
                if (connection.authLevel == 2 && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Administrators);
                }
                else if (connection.authLevel == 1 && !permission.UserHasGroup(playerId, defaultGroups.Moderators))
                {
                    permission.AddUserGroup(playerId, defaultGroups.Moderators);
                }
            }

            // Call hook for plugins
            object hookUniversal = Interface.CallHook("CanPlayerLogin", playerName, playerId, ipAddress);
            object hookDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanPlayerLogin", new DateTime(2020, 6, 1), playerName, playerId, ipAddress);
            object canLogin = hookUniversal ?? hookDeprecated;
            if (canLogin is string || canLogin is bool && !(bool)canLogin)
            {
                // Reject player with message
                ConnectionAuth.Reject(connection, canLogin is string ? canLogin.ToString() : lang.GetMessage("ConnectionRejected", this, playerId));
                return true;
            }

            // Let plugins know
            Interface.CallHook("OnPlayerApproved", playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApprove", new DateTime(2020, 6, 1), playerName, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2020, 6, 1), playerName, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player has been banned by Publisher/VAC
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="status"></param>
        [Hook("IOnPlayerBanned")]
        private void IOnPlayerBanned(Connection connection, AuthResponse status)
        {
            Interface.CallHook("OnPlayerBanned", connection, status.ToString());
        }

        /// <summary>
        /// Called when the player sends a chat message
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <param name="message"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(BasePlayer basePlayer, string message, Chat.ChatChannel channel)
        {
            // Handle the command
            if (Universal.CommandSystem.HandleChatMessage(basePlayer.IPlayer, message) == CommandState.Completed)
            {
                return true;
            }

            // Is the chat blocked?
            return Interface.CallHook("OnPlayerChat", basePlayer.IPlayer, message, channel);
        }

        /// <summary>
        /// Called when the player is authenticating
        /// </summary>
        /// <param name="connection"></param>
        private void OnClientAuth(Connection connection)
        {
            // Strip styling from username
            connection.username = Regex.Replace(connection.username, @"<[^>]*>", string.Empty); // TODO: Move regex pattern outside of hook and optimize
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="basePlayer"></param>
        [Hook("IOnPlayerConnected")]
        private void IOnPlayerConnected(BasePlayer basePlayer)
        {
            // Set default language for player if not set
            if (string.IsNullOrEmpty(lang.GetLanguage(basePlayer.UserIDString)))
            {
                lang.SetLanguage(basePlayer.net.connection.info.GetString("global.language", "en"), basePlayer.UserIDString);
            }

            Players.PlayerConnected(basePlayer.UserIDString, basePlayer);

            IPlayer player = Players.FindPlayerById(basePlayer.UserIDString);
            if (player != null)
            {
                // Set IPlayer object in BasePlayer
                basePlayer.IPlayer = player;

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnPlayerInit", "OnPlayerConnected", new DateTime(2020, 6, 1), player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2020, 6, 1), player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <param name="reason"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(BasePlayer basePlayer, string reason)
        {
            IPlayer player = basePlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", player, reason);
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2020, 6, 1), player, reason);
            }

            Players.PlayerDisconnected(basePlayer.UserIDString);
        }

        /// <summary>
        /// Called when setting/changing info values for a player
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        [Hook("OnPlayerSetInfo")]
        private void OnPlayerSetInfo(Connection connection, string key, string val)
        {
            // Change language for player
            if (key == "global.language")
            {
                lang.SetLanguage(val, connection.userid.ToString());
            }
        }

        /// <summary>
        /// Called when the player has been kicked
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <param name="reason"></param>
        [Hook("OnPlayerKicked")]
        private void OnPlayerKicked(BasePlayer basePlayer, string reason)
        {
            IPlayer player = basePlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerKicked", player, reason);
                Interface.CallDeprecatedHook("OnUserKicked", "OnPlayerKicked", new DateTime(2020, 6, 1), player);
            }
        }

        /// <summary>
        /// Called when the player is respawning
        /// </summary>
        /// <param name="basePlayer"></param>
        /// <returns></returns>
        [Hook("OnPlayerRespawn")]
        private object OnPlayerRespawn(BasePlayer basePlayer)
        {
            IPlayer player = basePlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                object respawnUniversal = Interface.CallHook("OnPlayerRespawn", player);
                object respawnDeprecated = Interface.CallDeprecatedHook("OnUserRespawn", "OnPlayerRespawn", new DateTime(2020, 6, 1), player);
                return respawnUniversal ?? respawnDeprecated;
            }

            return null;
        }

        /// <summary>
        /// Called when the player has respawned
        /// </summary>
        /// <param name="basePlayer"></param>
        [Hook("OnPlayerRespawned")]
        private void OnPlayerRespawned(BasePlayer basePlayer)
        {
            IPlayer player = basePlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerRespawned", player);
                Interface.CallDeprecatedHook("OnUserRespawned", "OnPlayerRespawned", new DateTime(2020, 6, 1), player);
            }
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a remote console command is received
        /// </summary>
        /// <returns></returns>
        /// <param name="ipAddress"></param>
        /// <param name="command"></param>
        [Hook("IOnRconCommand")]
        private object IOnRconCommand(IPAddress ipAddress, string command)
        {
            string[] fullCommand = CommandLine.Split(command);
            if (fullCommand.Length >= 1)
            {
                string cmd = fullCommand[0].ToLower();
                string[] args = fullCommand.Skip(1).ToArray();

                // Call hook for plugins
                if (Interface.CallHook("OnRconCommand", ipAddress, cmd, args) != null)
                {
                    return true;
                }
            }

            return null;
        }

        /// <summary>
        /// Called when a server command was run
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(ConsoleSystem.Arg arg)
        {
            if (arg == null || arg.Connection != null && arg.Player() == null)
            {
                return true; // Ignore console commands from client during connection
            }

            if (arg.cmd.FullName == "chat.say")
            {
                return null; // Skip chat commands, those are handled elsewhere
            }

            IPlayer player = arg.Player()?.IPlayer;
            if (player != null)
            {
                // Handle the command
                if (Universal.CommandSystem.HandleConsoleMessage(player, arg.FullString) == CommandState.Completed)
                {
                    return true;
                }
            }

            // Call hook for plugins
            return Interface.CallHook("OnServerCommand", arg.cmd.FullName, arg.Args);
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this, (player, args) =>
            {
                if (player.IsServer)
                {
                    player.Reply($"Protocol: {Server.Protocol}\nBuild Date: {BuildInfo.Current.BuildDate}\n" +
                                 $"Unity Version: {UnityEngine.Application.unityVersion}\nChangeset: {BuildInfo.Current.Scm.ChangeId}\n" +
                                 $"Branch: {BuildInfo.Current.Scm.Branch}\nuMod.Rust Version: {RustExtension.AssemblyVersion}");
                    return CommandState.Completed;
                }

                return CommandState.Unrecognized;
            });

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            /*if (!Interface.uMod.Config.Options.Modded)
            {
                Interface.uMod.LogWarning("The server is currently listed under Community. Please be aware that Facepunch only allows admin tools" +
                  " (that do not affect gameplay or make the server appear modded) under the Community section"); // TODO: Localization
            }*/ // TODO: Re-implement with configuration extension

            serverInitialized = true;
        }

        /// <summary>
        /// Called when the server description is updating
        /// </summary>
        [Hook("IOnUpdateServerDescription")]
        private void IOnUpdateServerDescription()
        {
            // Fix for server description not always updating
            SteamServer.SetKey("description_0", string.Empty);
        }

        /// <summary>
        /// Called when the server is updating Steam information
        /// </summary>
        [Hook("IOnUpdateServerInformation")]
        private void IOnUpdateServerInformation()
        {
            // Add Steam tags for uMod
            SteamServer.GameTags += ",umod,modded";
            /*if (Interface.uMod.Config.Options.Modded)
            {
                SteamServer.GameTags += ",modded";
            }*/ // TODO: Re-implement with configuration extension
        }

        #endregion Server Hooks

        #region Deprecated Hooks

        [Hook("IOnActiveItemChange")]
        private object IOnActiveItemChange(BasePlayer player, Item oldItem, uint newItemId)
        {
            object newHook = Interface.uMod.CallHook("OnActiveItemChange", player, oldItem, newItemId);
            object oldHook = Interface.uMod.CallDeprecatedHook("OnActiveItemChange", "OnActiveItemChange(BasePlayer player, Item oldItem, uint newItemId)",
                new DateTime(2020, 6, 1), player, newItemId);
            return newHook ?? oldHook;
        }

        [Hook("IOnActiveItemChanged")]
        private void IOnActiveItemChanged(BasePlayer player, Item oldItem, Item newItem)
        {
            Interface.uMod.CallHook("OnActiveItemChanged", player, oldItem, newItem);
            Interface.uMod.CallDeprecatedHook("OnPlayerActiveItemChanged", "OnActiveItemChanged(BasePlayer player, Item oldItem, Item newItem)",
                new DateTime(2020, 6, 1), player, oldItem, newItem);
        }

        [Hook("IOnPlayerConnectedOld")]
        private void IOnPlayerConnectedOld(Message packet)
        {
            Interface.uMod.CallDeprecatedHook("OnPlayerConnected", "OnPlayerConnected(BasePlayer player)",
                new DateTime(2020, 6, 1), packet);
        }

        [Hook("OnEntityKill")]
        private object OnEntityKill(CH47HelicopterAIController heli)
        {
            return Interface.uMod.CallDeprecatedHook("OnHelicopterKilled", "OnEntityKill(CH47HelicopterAIController heli)",
                new DateTime(2020, 6, 1), heli);
        }

        [Hook("OnNpcAttack")]
        private object OnNpcAttack(BaseNpc npc)
        {
            return Interface.uMod.CallDeprecatedHook("CanNpcAttack", "OnNpcAttack(BaseNpc npc)",
                new DateTime(2020, 6, 1), npc);
        }

        [Hook("OnNpcResume")]
        private object OnNpcResume(NPCPlayerApex npc)
        {
            return Interface.uMod.CallDeprecatedHook("OnNpcPlayerResume", "OnNpcResume(NPCPlayerApex npc)",
                new DateTime(2020, 6, 1), npc);
        }

        [Hook("OnPlayerDeath")]
        private object OnPlayerDeath(BasePlayer player, HitInfo hitInfo)
        {
            return Interface.uMod.CallDeprecatedHook("OnPlayerDie", "OnPlayerDeath(BasePlayer player, HitInfo hitInfo)",
                new DateTime(2020, 6, 1), player, hitInfo);
        }

        [Hook("OnQuarryToggled")]
        private void OnQuarryToggled(MiningQuarry quarry)
        {
            Interface.uMod.CallDeprecatedHook("OnQuarryEnabled", "OnQuarryToggled(MiningQuarry quarry)",
                new DateTime(2020, 6, 1), quarry);
        }

        #endregion Deprecated Hooks
    }
}

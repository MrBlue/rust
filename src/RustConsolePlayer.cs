﻿using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;

namespace uMod.Rust
{
    /// <summary>
    /// The player object that represents the server console
    /// </summary>
    public class RustConsolePlayer : ConsolePlayer, IPlayer
    {
        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object => null;

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get => CommandType.Console; set { } }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get => "Server Console"; set { } }

        /// <summary>
        /// Gets the player's language
        /// </summary>
        public CultureInfo Language => CultureInfo.InstalledUICulture;

        /// <summary>
        /// Gets the player's IP address
        /// </summary>
        public string Address => "127.0.0.1";

        /// <summary>
        /// Gets the player's average network ping
        /// </summary>
        public int Ping => 0;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => false;

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => true;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => true;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => false;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => false;

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => true;

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason, TimeSpan duration = default)
        {
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health { get; set; }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth { get; set; }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill()
        {
        }

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => new Position(0, 0, 0);

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            x = 0;
            y = 0;
            z = 0;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos)
        {
        }

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = args.Length > 0 ? string.Format(Formatter.ToPlaintext(message), args) : Formatter.ToPlaintext(message);
                Interface.uMod.LogInfo(prefix != null ? $"{prefix} {message}" : message);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command))
            {
                ConsoleSystem.Run(ConsoleSystem.Option.Server, command, args);
            }
        }

        #endregion Chat and Commands
    }
}

using System;
using System.Globalization;
using UnityEngine;

namespace uMod.Rust
{
    [Obsolete("Use universal helpers instead")]
    public class RustCore : Rust
    {
        /// <summary>
        /// Returns the BasePlayer for the specified name, ID, or IP address string
        /// </summary>
        /// <param name="nameOrIdOrIp"></param>
        /// <returns></returns>
        [Obsolete("Use Players.FindPlayer instead")]
        public static BasePlayer FindPlayer(string nameOrIdOrIp)
        {
            BasePlayer player = null;
            foreach (BasePlayer activePlayer in BasePlayer.activePlayerList)
            {
                if (string.IsNullOrEmpty(activePlayer.UserIDString))
                {
                    continue;
                }

                if (activePlayer.UserIDString.Equals(nameOrIdOrIp))
                {
                    return activePlayer;
                }

                if (string.IsNullOrEmpty(activePlayer.displayName))
                {
                    continue;
                }

                if (activePlayer.displayName.Equals(nameOrIdOrIp, StringComparison.OrdinalIgnoreCase))
                {
                    return activePlayer;
                }

                if (activePlayer.displayName.Contains(nameOrIdOrIp, CompareOptions.OrdinalIgnoreCase))
                {
                    player = activePlayer;
                }

                if (activePlayer.net?.connection != null && activePlayer.net.connection.ipaddress.Equals(nameOrIdOrIp))
                {
                    return activePlayer;
                }
            }
            foreach (BasePlayer sleepingPlayer in BasePlayer.sleepingPlayerList)
            {
                if (string.IsNullOrEmpty(sleepingPlayer.UserIDString))
                {
                    continue;
                }

                if (sleepingPlayer.UserIDString.Equals(nameOrIdOrIp))
                {
                    return sleepingPlayer;
                }

                if (string.IsNullOrEmpty(sleepingPlayer.displayName))
                {
                    continue;
                }

                if (sleepingPlayer.displayName.Equals(nameOrIdOrIp, StringComparison.OrdinalIgnoreCase))
                {
                    return sleepingPlayer;
                }

                if (sleepingPlayer.displayName.Contains(nameOrIdOrIp, CompareOptions.OrdinalIgnoreCase))
                {
                    player = sleepingPlayer;
                }
            }
            return player;
        }

        /// <summary>
        /// Returns the BasePlayer for the specified name string
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Obsolete("Use Players.FindPlayer instead")]
        public static BasePlayer FindPlayerByName(string name)
        {
            BasePlayer player = null;
            foreach (BasePlayer activePlayer in BasePlayer.activePlayerList)
            {
                if (string.IsNullOrEmpty(activePlayer.displayName))
                {
                    continue;
                }

                if (activePlayer.displayName.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return activePlayer;
                }

                if (activePlayer.displayName.Contains(name, CompareOptions.OrdinalIgnoreCase))
                {
                    player = activePlayer;
                }
            }
            foreach (BasePlayer sleepingPlayer in BasePlayer.sleepingPlayerList)
            {
                if (string.IsNullOrEmpty(sleepingPlayer.displayName))
                {
                    continue;
                }

                if (sleepingPlayer.displayName.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return sleepingPlayer;
                }

                if (sleepingPlayer.displayName.Contains(name, CompareOptions.OrdinalIgnoreCase))
                {
                    player = sleepingPlayer;
                }
            }
            return player;
        }

        /// <summary>
        /// Returns the BasePlayer for the specified ID ulong
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Obsolete("Use Players.FindPlayer instead")]
        public static BasePlayer FindPlayerById(ulong id)
        {
            foreach (BasePlayer activePlayer in BasePlayer.activePlayerList)
            {
                if (activePlayer.userID == id)
                {
                    return activePlayer;
                }
            }
            foreach (BasePlayer sleepingPlayer in BasePlayer.sleepingPlayerList)
            {
                if (sleepingPlayer.userID == id)
                {
                    return sleepingPlayer;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the BasePlayer for the specified ID string
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Obsolete("Use Players.FindPlayer instead")]
        public static BasePlayer FindPlayerByIdString(string id)
        {
            foreach (BasePlayer activePlayer in BasePlayer.activePlayerList)
            {
                if (string.IsNullOrEmpty(activePlayer.UserIDString))
                {
                    continue;
                }

                if (activePlayer.UserIDString.Equals(id))
                {
                    return activePlayer;
                }
            }
            foreach (BasePlayer sleepingPlayer in BasePlayer.sleepingPlayerList)
            {
                if (string.IsNullOrEmpty(sleepingPlayer.UserIDString))
                {
                    continue;
                }

                if (sleepingPlayer.UserIDString.Equals(id))
                {
                    return sleepingPlayer;
                }
            }

            return null;
        }
    }
}

namespace uMod.Plugins
{
    [Obsolete("Use Plugin instead")]
    public class RustPlugin : Plugin
    {
        protected Rust.Libraries.Command cmd = Interface.uMod.GetLibrary<Rust.Libraries.Command>();
        protected Rust.Libraries.Player Player = Interface.uMod.GetLibrary<Rust.Libraries.Player>();
        protected Rust.Libraries.Rust rust = Interface.uMod.GetLibrary<Rust.Libraries.Rust>();

        [Obsolete("Use Players.FindPlayer instead")]
        public BasePlayer FindPlayer(string nameOrIdOrIp)
        {
            BasePlayer basePlayer = Players.FindPlayer(nameOrIdOrIp)?.Object as BasePlayer;
            if (basePlayer?.net?.connection != null && basePlayer.net.connection.ipaddress.Equals(nameOrIdOrIp))
            {
                return basePlayer;
            }

            return null;
        }

        [Obsolete("Use Players.FindPlayerById instead")]
        public BasePlayer FindPlayerById(ulong playerId)
        {
            return FindPlayerByIdString(playerId.ToString());
        }

        [Obsolete("Use Players.FindPlayerById instead")]
        public BasePlayer FindPlayerByIdString(string playerId)
        {
            return Players.FindPlayerById(playerId)?.Object as BasePlayer;
        }

        [Obsolete("Use Players.FindPlayer instead")]
        public BasePlayer FindPlayerByName(string playerName)
        {
            return FindPlayer(playerName);
        }

        [Obsolete("Use IPlayer.Message instead")]
        protected void PrintToConsole(BasePlayer basePlayer, string message, params object[] args)
        {
            if (basePlayer?.net != null)
            {
                basePlayer.SendConsoleCommand("echo " + (args.Length > 0 ? string.Format(message, args) : message));
            }
        }

        [Obsolete("Use IPlayer.Message instead")]
        protected void PrintToConsole(string message, params object[] args)
        {
            if (BasePlayer.activePlayerList.Count >= 1)
            {
                ConsoleNetwork.BroadcastToAllClients("echo " + (args.Length > 0 ? string.Format(message, args) : message));
            }
        }

        [Obsolete("Use IPlayer.Message instead")]
        protected void PrintToChat(BasePlayer basePlayer, string message, params object[] args)
        {
            if (basePlayer?.net != null)
            {
                basePlayer.SendConsoleCommand("chat.add", 2, global::uMod.Rust.Rust.chatAvatar, args.Length > 0 ? string.Format(message, args) : message); // TODO: Implement setting for avatar
            }
        }

        [Obsolete("Use Server.Broadcast instead")]
        protected void PrintToChat(string message, params object[] args)
        {
            if (BasePlayer.activePlayerList.Count >= 1)
            {
                ConsoleNetwork.BroadcastToAllClients("chat.add", 2, global::uMod.Rust.Rust.chatAvatar, args.Length > 0 ? string.Format(message, args) : message); // TODO: Implement setting for avatar
            }
        }

        [Obsolete("Use IPlayer.Reply instead")]
        protected void SendReply(ConsoleSystem.Arg arg, string message, params object[] args)
        {
            BasePlayer basePlayer = arg.Connection?.player as BasePlayer;
            string formatted = args.Length > 0 ? string.Format(message, args) : message;

            if (basePlayer?.net != null)
            {
                basePlayer.SendConsoleCommand("echo " + formatted);
                return;
            }

            Interface.uMod.LogInfo(formatted);
        }

        [Obsolete("Use IPlayer.Reply instead")]
        protected void SendReply(BasePlayer basePlayer, string message, params object[] args)
        {
            PrintToChat(basePlayer, message, args);
        }

        [Obsolete("Use IPlayer.Message instead and/or LogWarning")]
        protected void SendWarning(ConsoleSystem.Arg arg, string message, params object[] args)
        {
            BasePlayer basePlayer = arg.Connection?.player as BasePlayer;
            string formatted = args.Length > 0 ? string.Format(message, args) : message;

            if (basePlayer?.net != null)
            {
                basePlayer.SendConsoleCommand("echo " + formatted);
                return;
            }

            Interface.uMod.LogWarning(formatted);
        }

        [Obsolete("Use IPlayer.Message instead and/or LogError")]
        protected void SendError(ConsoleSystem.Arg arg, string message, params object[] args)
        {
            BasePlayer basePlayer = arg.Connection?.player as BasePlayer;
            string formatted = args.Length > 0 ? string.Format(message, args) : message;

            if (basePlayer?.net != null)
            {
                basePlayer.SendConsoleCommand("echo " + formatted);
                return;
            }

            Interface.uMod.LogError(formatted);
        }

        [Obsolete("Use IPlayer.Teleport instead")]
        protected void ForcePlayerPosition(BasePlayer basePlayer, Vector3 destination)
        {
            basePlayer.IPlayer.Teleport(destination.x, destination.y, destination.z);
        }
    }
}

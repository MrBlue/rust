using System;
using System.Globalization;
using System.Text.RegularExpressions;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using uMod.Unity;
using UnityEngine;

namespace uMod.Rust
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class RustPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private const string ipPattern = @":{1}[0-9]{1}\d*";

        private readonly BasePlayer player;
        private readonly ulong steamId;

        public RustPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
            ulong.TryParse(playerId, out steamId);
        }

        public RustPlayer(ulong playerId, string playerName)
        {
            // Store player details
            Id = playerId.ToString();
            Name = playerName.Sanitize();
            steamId = playerId;
        }

        public RustPlayer(BasePlayer player) : this(player.userID, player.displayName)
        {
            // Store player object(s)
            this.player = player;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object => player;

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo(player != null ? player?.net?.connection?.info.GetString("global.language") : "en");

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => player?.net?.connection != null ? Regex.Replace(player.net.connection.ipaddress, ipPattern, "") : string.Empty;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => player?.net?.connection != null ? Network.Net.sv.GetAveragePing(player.net.connection) : 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => ServerUsers.Is(steamId, ServerUsers.UserGroup.Owner);

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => ServerUsers.Is(steamId, ServerUsers.UserGroup.Moderator);

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => ServerUsers.Is(steamId, ServerUsers.UserGroup.Banned);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => player != null ? player.IsConnected : BasePlayer.FindByID(steamId) != null;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => player != null && player.IsAlive();

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => player != null && player.IsDead();

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => BasePlayer.FindSleeping(steamId) != null;

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player
                ServerUsers.Set(steamId, ServerUsers.UserGroup.Banned, player?.displayName ?? "Unknown", reason); // TODO: Localization
                ServerUsers.Save();

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                player.Kick(reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                ServerUsers.Remove(steamId);
                ServerUsers.Save();
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => player != null ? player.health : 0f;
            set
            {
                if (player != null)
                {
                    player.health = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => player != null ? player.MaxHealth() : 0f;
            set
            {
                if (player != null)
                {
                    player._maxHealth = value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (player != null)
            {
                player.StopWounded();
                player.Heal(amount);
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount) => player?.Hurt(amount);

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => player?.Die();

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName)
        {
            if (player?.net?.connection != null)
            {
                // Clean up name
                newName = string.IsNullOrEmpty(newName.Trim()) ? player.displayName : newName;

                // Update name in-game
                player.net.connection.username = newName;
                player.displayName = newName;
                player._name = newName;
                player.SendNetworkUpdateImmediate();

                // Update name in uMod
                player.IPlayer.Name = newName;
            }
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            if (player != null)
            {
                player.StopWounded();
                player.metabolism.Reset();
                player.metabolism.SendChangesToClient();
            }
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn() => player?.Respawn();

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            if (player != null)
            {
                player.RespawnAt(pos.ToVector3(), new Quaternion());
            }
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => player?.transform?.position.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && player.IsAlive() && !player.IsSpectating())
            {
                try
                {
                    Vector3 destination = new Vector3(x, y, z);

                    // Dismount and remove parent, if applicable
                    player.EnsureDismounted();
                    player.SetParent(null, true, true);

                    // Prevent player from getting hurt
                    //player.SetPlayerFlag(BasePlayer.PlayerFlags.ReceivingSnapshot, true);
                    //player.UpdatePlayerCollider(true);
                    //player.UpdatePlayerRigidbody(false);
                    player.EnableServerFall(true);

                    // Teleport the player to position
                    player.MovePosition(destination);
                    player.ClientRPCPlayer(null, player, "ForcePositionTo", destination);

                    // Update network group if outside current group
                    /*if (!player.net.sv.visibility.IsInside(player.net.group, destination))
                    {
                        player.UpdateNetworkGroup();
                        player.SendNetworkUpdateImmediate();
                        player.ClearEntityQueue();
                        player.SendFullSnapshot();
                    }*/
                }
                finally
                {
                    // Restore player behavior
                    //player.UpdatePlayerCollider(true);
                    //player.UpdatePlayerRigidbody(true);
                    player.EnableServerFall(false);
                }
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToUnity(message), args) : Formatter.ToUnity(message);
                player.SendConsoleCommand("chat.add", 2, Rust.chatAvatar, prefix != null ? $"{prefix} {message}" : message); // TODO: Implement setting for avatar
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                switch (LastCommand)
                {
                    case CommandType.Chat:
                        Message(message, prefix, args);
                        break;

                    case CommandType.Console:
                        player.ConsoleMessage(string.Format(Formatter.ToPlaintext(message), args));
                        break;
                }
            }
        }

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Reply(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                player.SendConsoleCommand(command, args);
            }
        }

        #endregion Chat and Commands

        #region Item Handling

        /// <summary>
        /// Drops item by item ID from player's inventory
        /// </summary>
        /// <param name="itemId"></param>
        /*public void DropItem(int itemId)
        {
            Vector3 position = player.transform.position;
            PlayerInventory inventory = Inventory(player);
            for (int s = 0; s < inventory.containerMain.capacity; s++)
            {
                global::Item i = inventory.containerMain.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerBelt.capacity; s++)
            {
                global::Item i = inventory.containerBelt.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerWear.capacity; s++)
            {
                global::Item i = inventory.containerWear.GetSlot(s);
                if (i.info.itemid == itemId)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
        }*/

        /// <summary>
        /// Drops item from the player's inventory
        /// </summary>
        /// <param name="item"></param>
        /*public void DropItem(global::Item item)
        {
            Vector3 position = player.transform.position;
            PlayerInventory inventory = Inventory(player);
            for (int s = 0; s < inventory.containerMain.capacity; s++)
            {
                global::Item i = inventory.containerMain.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerBelt.capacity; s++)
            {
                global::Item i = inventory.containerBelt.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
            for (int s = 0; s < inventory.containerWear.capacity; s++)
            {
                global::Item i = inventory.containerWear.GetSlot(s);
                if (i == item)
                {
                    i.Drop(position + new Vector3(0f, 1f, 0f) + position / 2f, (position + new Vector3(0f, 0.2f, 0f)) * 8f);
                }
            }
        }*/

        /// <summary>
        /// Gives quantity of an item to the player
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="quantity"></param>
        //public void GiveItem(int itemId, int quantity = 1) => GiveItem(player, Item.GetItem(itemId), quantity);

        /// <summary>
        /// Gives quantity of an item to the player
        /// </summary>
        /// <param name="item"></param>
        /// <param name="quantity"></param>
        //public void GiveItem(global::Item item, int quantity = 1) => player.inventory.GiveItem(ItemManager.CreateByItemID(item.info.itemid, quantity));

        #endregion Item Handling

        #region Inventory Handling

        /// <summary>
        /// Gets the inventory of the player
        /// </summary>
        /// <returns></returns>
        //public PlayerInventory Inventory() => player.inventory;

        /// <summary>
        /// Clears the inventory of the player
        /// </summary>
        //public void ClearInventory() => Inventory(player)?.Strip();

        /// <summary>
        /// Resets the inventory of the player
        /// </summary>
        /*public void ResetInventory()
        {
            PlayerInventory inventory = Inventory(player);
            if (inventory != null)
            {
                inventory.DoDestroy();
                inventory.ServerInit(player);
            }
        }*/

        #endregion Inventory Handling
    }
}

﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Rust
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class RustPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Rust) };

        public RustPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
